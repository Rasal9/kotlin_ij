const val HERO_NAME = "Вася"
var artifact = false
fun main(args: Array<String>) {
    println("Наш герой готов к новым приключениям!")
    println("Имя героя: $HERO_NAME")

    val playerGuild = readLineGuild()

    val friendlyWithBarbarians = readLineFriendship()
    val angeredBarbarians = readLineAngered()

    var level = readLineLevel()
    var quest = obtainQuest(level, angeredBarbarians, friendlyWithBarbarians, playerGuild)

    println("Герой приблизился к доске объявлений и прочитал:")

    println(quest)

    //println("Время идет ...")


    quest = obtainQuest(level, false, false, playerGuild)

    var meetingWithMartinCompleted = false

    if (quest == "Найти волшебника Мартина" && !meetingWithMartinCompleted) {
        artifact = meetingWithMartin(HERO_NAME, playerGuild)
        meetingWithMartinCompleted = true
    }

    if (quest == "Встреча со старыми друзьями"){
        visitTavern()
    }



    if (quest == "Сечас квестов нету") {
        println("Уровень героя не изменился $level")
    } else {
        println("Герой возвращается после выполнения квеста.\nУровень героя повышается")
        level += 1
        println("Новый уровень героя: $level")

        println("Герой получил новый квест и пошел читать его название")
    }
}


private fun readLineGuild(): String {
    println("$HERO_NAME, какую гильдию выбираете?")
    println("Гильдии: маг, купец, Ремесленник и варвор")
    return readLine()?.toLowerCase() ?: ""
}

private fun readLineFriendship(): Boolean {
    println("Вы дружите с варварами? true - да, false - нет")
    val input = readLine()
    return input?.toBoolean() ?: false
}

private fun readLineAngered(): Boolean {
    println("Вы разгневали варваров до начала игры? true - да, false - нет")
    val input = readLine()
    return input?.toBoolean() ?: false
}

private fun readLineLevel(): Int {
    println("$HERO_NAME, выберите начальный уровень игры")
    val levl = readLine()
    return levl?.toIntOrNull() ?: 1
}

private fun obtainQuest(
    playerLevel: Int, hasAngeredBarbarians: Boolean, hasBeFriendedBarbarians: Boolean, playerClass: String

): String {
    val queue: String = when (playerLevel) {
        1 -> "Встречаться с гномом на горной тропе"
        in 2..5 -> {
            val canTalkoBarbarinas = !hasAngeredBarbarians && (hasBeFriendedBarbarians || playerClass == "barbarian")

            if (canTalkoBarbarinas) {
                "Убедить врагов прекратить вторжение"
            } else {
                "Герой должен спасти город от вторжения врагов"
            }
        }

        6 -> "Найти зачарованный меч"
        7 -> "Вернуть потерянный артефакт"
        8 -> "Найти волшебника Мартина"
        10 -> "Спать"
        11 -> "Отдать кольцо"
        12 -> "Найти меч"
        13 -> "Встреча со старыми друзьями"
        14 -> "Приручить дракона"
        15 -> "Отдохнуть "
        else -> "Сейчас квестов нет"

    }
    return queue
}

private fun meetingWithMartin(heroName: String, playerGuild: String): Boolean {
    return when (playerGuild) {
        "Маг" -> {
            println("$heroName, у тебя такой высокий уровень магии. Я отдам тебе свиток с заклянанием для вызова фамильяра")
            true
        }

        "Купец" -> {
            println("$heroName, заклинание увеличения прибыли стоит 50 монет. У тебя есть 50 монет?")
            val answer = readLine()?.toLowerCase()
            if (answer == "Да") {
                println("Я продам тебе свиток с заклинанием 'Неделя удачи'")
                true
            } else {
                println("Когда у тебя будет 50 монет, тогда я продам тебе свиток с заклинанием")
                false
            }
        }

        "Ремесленник" -> {
            println("$heroName, ты будешь раз в неделю доставлять мне товары бесплатно?")
            val answer = readLine()?.toLowerCase()
            if (answer == "Да") {
                println("Спасибо, тогда я подарю тебе свиток с заклянанием 'Удача в делах'")
                true
            } else {
                println("Тогда и я тебе ничего не дам")
                false
            }
        }

        "Варвар" -> {
            println("$heroName, у меня для тебя ничего нет")
            false
        }

        else -> false
    }
}